package br.ucsal._20202.testequalidade.aula03.business;

public class CadernetaBO {

	private CadernetaBO() {
	}

	public static String calcularSituacao(Double media) {
		if (media > 10 || media < 0) {
			return "Média inválida";
		}
		if (media >= 6) {
			return "Aprovado";
		}
		if (media >= 3) {
			return "Prova final";
		}
		return "Reprovado";
	}

}
