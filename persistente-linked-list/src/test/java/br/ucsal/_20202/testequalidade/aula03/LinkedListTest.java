package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

class LinkedListTest {

	private List<String> linkedList;

	@BeforeEach
	void setup() {
		linkedList = new LinkedList<>();
	}

	@Test
	void testarAdd1Nome() throws InvalidElementException {
		// Dado de entrada
		String nome = "Claudio Neiva";

		// Saída esperada
		String nomeEsperado = nome;

		// Executar o método que vc quer testar (add)
		linkedList.add(nome);

		// Obter a saída atual (get)
		String nomeAtual = linkedList.get(0);

		// Comparar a saída esperada com a saída atual
		Assertions.assertAll("adicionar 1 nome", () -> Assertions.assertEquals(nomeEsperado, nomeAtual),
				() -> Assertions.assertEquals(1, linkedList.size()));
	}

	@Test
	void testarAdd3Nomes() throws InvalidElementException {
		// Dado de entrada
		String nome1 = "Claudio Neiva";
		String nome2 = "Alisson Oliveira";
		String nome3 = "Guilherme Bacelar";

		// Executar o método que vc quer testar (add)
		linkedList.add(nome1);
		linkedList.add(nome2);
		linkedList.add(nome3);

		// Obter a saída atual (get)
		String nomeAtual1 = linkedList.get(0);
		String nomeAtual2 = linkedList.get(1);
		String nomeAtual3 = linkedList.get(2);

		// Comparar a saída esperada com a saída atual
		Assertions.assertAll("adicionar 3 nomes", () -> Assertions.assertEquals(nome1, nomeAtual1),
				() -> Assertions.assertEquals(nome2, nomeAtual2), () -> Assertions.assertEquals(nome3, nomeAtual3),
				() -> Assertions.assertEquals(3, linkedList.size()));
	}

	@Test
	void testarAddElementoInvalido() {
		String nome = null;
		String mensagemEsperada = "The element can't be null.";
		InvalidElementException exceptionLancada = Assertions.assertThrows(InvalidElementException.class,
				() -> linkedList.add(nome));
		Assertions.assertEquals(mensagemEsperada, exceptionLancada.getMessage());
	}

	@Test
	void testarGetElementoForaLimites() throws InvalidElementException {
		linkedList.add("claudio"); // índice 0
		linkedList.add("neiva"); // índice 1
		Assertions.assertNull(linkedList.get(2));
		
	}
	
}
