package br.ucsal._20202.testequalidade.aula03.domain;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class AlunoTest {

	@ParameterizedTest
	@MethodSource("gerarAlunosTest")
	void testarClonagem(Aluno aluno) {
		Aluno cloneAluno = aluno.clonar();
		Assertions.assertEquals(aluno, cloneAluno);
		Assertions.assertNotSame(aluno, cloneAluno);
	}

	private static Stream<Arguments> gerarAlunosTest() {
		Aluno aluno1 = new Aluno(123, "claudio", "rua x");
		Aluno aluno2 = new Aluno(null, "claudio", "rua x");
		Aluno aluno3 = new Aluno(123, null, "rua x");
		Aluno aluno4 = new Aluno(123, "claudio", null);
	    return Stream.of(
  	      Arguments.of(aluno1),
	      Arguments.of(aluno2),
	      Arguments.of(aluno3),
	      Arguments.of(aluno4)
	    );
	}
	
}
