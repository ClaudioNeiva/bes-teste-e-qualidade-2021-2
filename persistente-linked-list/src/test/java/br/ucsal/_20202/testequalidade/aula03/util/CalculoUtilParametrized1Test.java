package br.ucsal._20202.testequalidade.aula03.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CalculoUtilParametrized1Test {

	@ParameterizedTest(name = "{index}. calcularFatorial({0})={1}")
	@CsvSource({ "0,1", "1,1", "2,2", "3,6", "4,24", "5,120" })
	void testarFatorial(int n, long fatorialEsperado) throws Exception {
		CalculoUtil calculoUtil = new CalculoUtil();
		Assertions.assertEquals(fatorialEsperado, calculoUtil.calcularFatorial(n));
	}

	@Test
	void testarQualquerCoisa() {
		// Teste não parametrizado!
	}

}
