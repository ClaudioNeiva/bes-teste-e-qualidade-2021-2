package br.ucsal._20202.testequalidade.aula03.util;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CalculoUtilParametrized2Test {

	@ParameterizedTest(name = "{index}. calcularFatorial({0})={1}")
	@MethodSource("fornecerDadosTest")
	void testarFatorial(int n, long fatorialEsperado) throws Exception {
		CalculoUtil calculoUtil = new CalculoUtil();
		Assertions.assertEquals(fatorialEsperado, calculoUtil.calcularFatorial(n));
	}

	private static Stream<Arguments> fornecerDadosTest() {
	    return Stream.of(
	      Arguments.of(3, 6),
	      Arguments.of(0, 1),
	      Arguments.of(4, 24),
	      Arguments.of(1, 1)
	    );
	}

}
