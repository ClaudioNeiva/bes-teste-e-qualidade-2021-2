package br.ucsal._20202.testequalidade.aula03.util;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ExemploAnotacoesTest {

	 @BeforeAll
	 static void setupClasse() {
		 System.out.println("setupClasse...");
	 }
	 
	 @AfterAll
	 static void teardownClasse() {
		 System.out.println("teardownClasse...");
	 }
	 
	 @BeforeEach
	 void setup() {
		 System.out.println("	setup...");
	 }
	 
	 @AfterEach
	 void teardown() {
		 System.out.println("	teardown...");
	 }
	 
	 @Test
	 void testar1() {
		 System.out.println("		testar1...");
	 }
	 
	 @Test
	 @DisplayName("nome de exibição para o teste")
	 void testar2() {
		 System.out.println("		testar2...");
	 }

	 @Test
	 @Disabled
	 void testar3() {
		 System.out.println("		testar3...");
	 }

	 @Test
	 void testar4() {
		 System.out.println("		testar4...");
	 }
	 
}

