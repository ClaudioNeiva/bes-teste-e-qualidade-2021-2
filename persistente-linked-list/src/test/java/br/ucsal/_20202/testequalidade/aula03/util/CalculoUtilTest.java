package br.ucsal._20202.testequalidade.aula03.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculoUtilTest {

	CalculoUtil calculoUtil;

	@BeforeEach
	void setup() {
		calculoUtil = new CalculoUtil();
	}

	@Test
	void testarFatorial5() {
		// Dados de entrada
		int n = 5;
		// Resultado esperado
		long fatorialEsperado = 120;

		// Execução do método que está sendo testado
		// Coleta do resultado atual
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparação do resultado esperado com o resultado atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial0() {
		// Dados de entrada
		int n = 0;
		// Resultado esperado
		long fatorialEsperado = 1;

		// Execução do método que está sendo testado
		// Coleta do resultado atual
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparação do resultado esperado com o resultado atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial0E5E6() {
		int n1 = 0;
		long fatorialEsperado1 = 1;

		int n2 = 5;
		long fatorialEsperado2 = 120;

		long fatorialEsperado3 = 720;
		int n3 = 6;

		Assertions.assertAll("cálculos de fatorial",
				() -> assertEquals(fatorialEsperado2, calculoUtil.calcularFatorial(n2)),
				() -> assertEquals(fatorialEsperado3, calculoUtil.calcularFatorial(n3)),
				() -> assertEquals(fatorialEsperado1, calculoUtil.calcularFatorial(n1)));
	}

	@Test
	void testarFatorial0E5E6Solucao2() {
		int ns[] = { 0, 5, 6 };
		long fatoriaisEsperados[] = { 1, 120, 720 };
		for (int i = 0; i < ns.length; i++) {
			Long fatorialAtual = calculoUtil.calcularFatorial(ns[i]);
			assertEquals(fatoriaisEsperados[i], fatorialAtual);
		}
	}

//	@Test
//	void testarClonagem() {
//		Aluno alunoOriginal = new Aluno(123, "Claudio Neiva");
//		
//		Aluno alunoClone = alunoOriginal.clonar();
//		
//		Assertions.assertEquals(alunoClone, alunoOriginal, "os objetos não têm o mesmo valor para os atributos");
//		Assertions.assertNotSame(alunoClone, alunoOriginal, "não foram criados dois objetos, pois as variáveis apontam para a mesma instância");
//	}
//	
}
