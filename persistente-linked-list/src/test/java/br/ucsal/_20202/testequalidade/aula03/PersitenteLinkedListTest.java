package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.infra.DbInfraUtil;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

class PersitenteLinkedListTest {

	@Test
	void testarPersistencia3Nomes() throws InvalidElementException, SQLException, IOException, ClassNotFoundException {

		Assumptions.assumeTrue(DbInfraUtil.isConnectionValid());

		// Dados de entrada
		String nome1 = "antonio";
		String nome2 = "claudio";
		String nome3 = "neiva";

		// Saída esperada
		// São os mesmos nomes 1, 2 e 3.

		PersistentList<String> persistentList = new PersitenteLinkedList<>();
		persistentList.add(nome1);
		persistentList.add(nome2);
		persistentList.add(nome3);

		// Executar o método que está sendo testado
		Long idList = 1L;
		Connection connection = DbUtil.getConnection();
		String tabName = "lista1";
		persistentList.persist(idList, connection, tabName);

		// Obter a saída atual
		PersistentList<String> persistentListAtual = new PersitenteLinkedList<>();
		persistentListAtual.load(idList, connection, tabName);

		// Comparar a saída esperada com a saída atual
		Assertions.assertEquals(nome1, persistentListAtual.get(0));
		Assertions.assertEquals(nome2, persistentListAtual.get(1));
		Assertions.assertEquals(nome3, persistentListAtual.get(2));
	}

}
