package br.ucsal.bes20212.testequalidade.aula10;

public class Classe1 {

	public static int metodo1() {
		return 1;
	}

	private static int metodo2() {
		return 2;
	}

	private int metodo3() {
		System.out.println("executando o método 3 original...");
		return 3;
	}

	public void metodo4() {
		System.out.println("metodo1()=" + metodo1());
		System.out.println("metodo2()=" + metodo2());
		System.out.println("metodo3()=" + metodo3());
		Classe3 objeto3 = new Classe3();
	}

	public int metodo5() {
		return metodo1() + metodo2() + metodo3();
	}

	private int metodo6() {
		return metodo1() + metodo2() + metodo3();
	}

	private int metodo7(int n1, int n2) {
		return n1 + n2;
	}

	public Integer totalizar(Integer... valores) {
		Integer total = 0;
		for (int i = 0; i < valores.length; i++) {
			total += valores[i];
		}
		return total;
	}

}
