package br.ucsal.bes20212.testequalidade.locadora.infra;

import java.util.List;

import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.testequalidade.locadora.persistence.VeiculoDAO;

//Nós NÃO faremos assim na vida real. Esta classe é apenas um exemplo de como trabalhar SEM o uso de um framework de apoio a testes.
public class VeiculoDAODuble extends VeiculoDAO {

	private List<Veiculo> veiculosConfigurados;

	public void configurarVeiculos(List<Veiculo> veiculosConfigurados) {
		this.veiculosConfigurados = veiculosConfigurados;
	}

	@Override
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		return veiculosConfigurados;
	}

}
