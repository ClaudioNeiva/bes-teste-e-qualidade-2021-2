package br.ucsal.bes20212.testequalidade.locadora.builder;

import java.time.LocalDate;

import br.ucsal.bes20212.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

// FIXME REMOVER essa classe, pois o uso do VeiculoBuilder é mais flexível.
public class VeiculoMother {

	private static final String PLACA_DEFAULT = "ABC-9854";
	private static final Modelo MODELO_DEFAULT = new Modelo("BMW");
	private static final Integer ANO_FABRICACAO_DEFAUL = 2021;
	private static final Double VALOR_DIARIA_DEFAULT = 500d;
	private static final SituacaoVeiculoEnum SITUACAO_VEICULO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;

	private VeiculoMother() {
	}

	public static Veiculo criarVeiculo() {
		return criarVeiculo(PLACA_DEFAULT, MODELO_DEFAULT, ANO_FABRICACAO_DEFAUL, VALOR_DIARIA_DEFAULT,
				SITUACAO_VEICULO_DEFAULT);
	}

	public static Veiculo criarVeiculoDisponivelFabricado6Anos(String placa) {
		Integer anoFabricacaoMais5Anos = LocalDate.now().getYear() - 6;
		return criarVeiculo(placa, MODELO_DEFAULT, anoFabricacaoMais5Anos, VALOR_DIARIA_DEFAULT,
				SituacaoVeiculoEnum.DISPONIVEL);
	}

	public static Veiculo criarVeiculoDisponivelFabricado1Ano(String placa) {
		Integer anoFabricacaoMais5Anos = LocalDate.now().getYear() - 1;
		Veiculo veiculo = criarVeiculo(placa, MODELO_DEFAULT, anoFabricacaoMais5Anos, VALOR_DIARIA_DEFAULT,
				SituacaoVeiculoEnum.DISPONIVEL);
		return veiculo;
	}

	private static Veiculo criarVeiculo(String placa, Modelo modelo, Integer anoFabricacao, Double valorDiaria,
			SituacaoVeiculoEnum situacao) {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;
	}

}
