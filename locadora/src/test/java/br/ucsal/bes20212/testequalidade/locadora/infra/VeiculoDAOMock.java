package br.ucsal.bes20212.testequalidade.locadora.infra;

import java.util.List;

import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.testequalidade.locadora.persistence.VeiculoDAO;

// ESSA CLASSE NÃO EXISTE. É só um "raciocínio"!
// Quando você solicitar a instaciação de um mock pelo mockito, pense que instância devolvida será baseada numa classe como essa:
public class VeiculoDAOMock extends VeiculoDAO {

	@Override
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		return null;
	}

	@Override
	public Veiculo obterPorPlaca(String placa) throws VeiculoNaoEncontradoException {
		return null;
	}

	@Override
	public void insert(Veiculo veiculo) {
	}

}
