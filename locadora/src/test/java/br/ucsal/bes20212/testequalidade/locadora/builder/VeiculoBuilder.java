package br.ucsal.bes20212.testequalidade.locadora.builder;

import br.ucsal.bes20212.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String PLACA_DEFAULT = "ABC-1234";
	private static final Integer ANO_FABRICACAO_DEFAULT = 2000;
	private static final Modelo MODELO_DEFAULT = new Modelo("Gol");
	private static final Double VALOR_DIARIA_DEFAULT = 100d;
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa = PLACA_DEFAULT;
	private Integer anoFabricacao = ANO_FABRICACAO_DEFAULT;
	private Modelo modelo = MODELO_DEFAULT;
	private Double valorDiaria = VALOR_DIARIA_DEFAULT;
	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;

	private VeiculoBuilder() {
	}

	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}

	public static VeiculoBuilder umVeiculoDisponivel() {
		return new VeiculoBuilder().disponivel();
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder fabricadoEm(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
		return this;
	}

	public VeiculoBuilder doModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder doModelo(String nomeModelo) {
		this.modelo = new Modelo(nomeModelo);
		return this;
	}

	public VeiculoBuilder comDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder emManutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder mas() {
		return new VeiculoBuilder().comPlaca(placa).fabricadoEm(anoFabricacao).doModelo(modelo).comDiaria(valorDiaria)
				.comSituacao(situacao);
	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setModelo(modelo);
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;
	}

}
