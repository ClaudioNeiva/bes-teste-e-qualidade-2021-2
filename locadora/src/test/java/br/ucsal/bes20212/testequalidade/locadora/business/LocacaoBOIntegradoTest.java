package br.ucsal.bes20212.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20212.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * Testes - INTEGRADOS
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOIntegradoTest {

	private VeiculoDAO veiculoDAO;

	private LocacaoBO locacaoBO;

	@BeforeEach
	void setup() {
		veiculoDAO = new VeiculoDAO();
		locacaoBO = new LocacaoBO(veiculoDAO);
	}

	/**
	 * Testar o cálculo do valor total de locação por 3 dias de 1 veículo fabricado em 2005.
	 */
	void testarCalculoValorTotalLocacao1Veiculo3Dias() {
	}

	/**
	 * Testar o cálculo do valor total de locação por 3 dias de 4 veículo fabricado há mais de 5 anos e veículo fabricado há 1 ano.
	 * 
	 * Dados de entrada:
	 * 
	 * 1. Prazo de locação = 3 dias
	 * 
	 * 2. Data de referência = data atual
	 * 
	 * 3. As placas dos veículos: 4 veículos fabricados há 6 anos + 1 veículo fabricado há 1 ano. Todos com diária de 100 reais;
	 * 
	 * Saída esperada:
	 * 
	 * 1. Valor da locação: 1.380,00 reais
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {

		// Dados de entrada

		int quantidadeDiasLocacao = 3;

		LocalDate dataReferencia = LocalDate.now();

		int anoFabricacaoAntigo = dataReferencia.getYear() - 6;
		int anoFabricacaoNovo = dataReferencia.getYear() - 1;

		VeiculoBuilder veiculoBuilderVeiculosAntigos = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(anoFabricacaoAntigo)
				.comDiaria(100d);

		Veiculo veiculo1 = veiculoBuilderVeiculosAntigos.comPlaca("ABC-1234").build();
		Veiculo veiculo2 = veiculoBuilderVeiculosAntigos.comPlaca("BCE-4566").build();
		Veiculo veiculo3 = veiculoBuilderVeiculosAntigos.comPlaca("ERT-3456").build();
		Veiculo veiculo4 = veiculoBuilderVeiculosAntigos.comPlaca("HGT-5611").build();
		Veiculo veiculo5 = veiculoBuilderVeiculosAntigos.comPlaca("PLM-2311").mas().fabricadoEm(anoFabricacaoNovo).build();

		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca(), veiculo3.getPlaca(), veiculo4.getPlaca(),
				veiculo5.getPlaca());

		// Saída esperada

		Double valorLocacaoEsperado = 1380d;

		// Preparação da base para execução dos testes

		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);
		veiculoDAO.insert(veiculo3);
		veiculoDAO.insert(veiculo4);
		veiculoDAO.insert(veiculo5);

		// Executar o método que está sendo testado e obter a saída atual

		Double valorLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataReferencia);

		// Comparar o resultado esperado com o resultado atual

		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);
	}

}

// Veiculo veiculo1 = VeiculoMother.criarVeiculoDisponivelFabricado6Anos("ABC-1234");
// Veiculo veiculo2 = VeiculoMother.criarVeiculoDisponivelFabricado6Anos("BCE-4566");
// Veiculo veiculo3 = VeiculoMother.criarVeiculoDisponivelFabricado6Anos("ERT-3456");
// Veiculo veiculo4 = VeiculoMother.criarVeiculoDisponivelFabricado6Anos("HGT-5611");
// Veiculo veiculo5 = VeiculoMother.criarVeiculoDisponivelFabricado1Ano("PLM-2311");
