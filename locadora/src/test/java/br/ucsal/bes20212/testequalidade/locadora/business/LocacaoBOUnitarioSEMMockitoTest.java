package br.ucsal.bes20212.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20212.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.testequalidade.locadora.infra.VeiculoDAODuble;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * Testes - UNITÁRIOS
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOUnitarioSEMMockitoTest {

	private VeiculoDAODuble veiculoDAODuble;

	private LocacaoBO locacaoBO;

	@BeforeEach
	void setup() {
		veiculoDAODuble = new VeiculoDAODuble();
		locacaoBO = new LocacaoBO(veiculoDAODuble);
	}

	/**
	 * Testar o cálculo do valor total de locação por 3 dias de 1 veículo fabricado em 2005.
	 */
	void testarCalculoValorTotalLocacao1Veiculo3Dias() {
	}

	/**
	 * Testar o cálculo do valor total de locação por 3 dias de 4 veículo fabricado há mais de 5 anos e veículo fabricado há 1 ano.
	 * 
	 * Dados de entrada:
	 * 
	 * 1. Prazo de locação = 3 dias
	 * 
	 * 2. Data de referência = data atual
	 * 
	 * 3. As placas dos veículos: 4 veículos fabricados há 6 anos + 1 veículo fabricado há 1 ano. Todos com diária de 100 reais;
	 * 
	 * Saída esperada:
	 * 
	 * 1. Valor da locação: 1.380,00 reais
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {

		// Dados de entrada

		int quantidadeDiasLocacao = 3;

		LocalDate dataReferencia = LocalDate.now();

		int anoFabricacaoAntigo = dataReferencia.getYear() - 6;
		int anoFabricacaoNovo = dataReferencia.getYear() - 1;

		VeiculoBuilder veiculoBuilderVeiculosAntigos = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(anoFabricacaoAntigo)
				.comDiaria(100d);

		Veiculo veiculo1 = veiculoBuilderVeiculosAntigos.comPlaca("ABC-1234").build();
		Veiculo veiculo2 = veiculoBuilderVeiculosAntigos.comPlaca("BCE-4566").build();
		Veiculo veiculo3 = veiculoBuilderVeiculosAntigos.comPlaca("ERT-3456").build();
		Veiculo veiculo4 = veiculoBuilderVeiculosAntigos.comPlaca("HGT-5611").build();
		Veiculo veiculo5 = veiculoBuilderVeiculosAntigos.comPlaca("PLM-2311").mas().fabricadoEm(anoFabricacaoNovo).build();

		veiculoDAODuble.configurarVeiculos(Arrays.asList(veiculo1, veiculo2, veiculo3, veiculo4, veiculo5));

		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca(), veiculo3.getPlaca(), veiculo4.getPlaca(),
				veiculo5.getPlaca());

		// Saída esperada

		Double valorLocacaoEsperado = 1380d;

		// Executar o método que está sendo testado e obter a saída atual

		// O método calcularValorTotalLocacao é do tipo query, ou seja, que tem retorno.
		Double valorLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataReferencia);

		// Comparar o resultado esperado com o resultado atual

		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);
	}

}
