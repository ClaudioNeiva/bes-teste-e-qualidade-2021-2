package br.ucsal.bes20212.testequalidade.aula12;

import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.chrome.ChromeDriver;

public class InvestingComChromeTest extends AbstractInvestingComTest {

	@BeforeEach
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver-91.0.4472.101");
		driver = new ChromeDriver();
	}

}
