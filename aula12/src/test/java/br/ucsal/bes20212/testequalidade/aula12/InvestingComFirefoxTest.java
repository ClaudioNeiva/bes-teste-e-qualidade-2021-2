package br.ucsal.bes20212.testequalidade.aula12;

import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InvestingComFirefoxTest extends AbstractInvestingComTest {

	@BeforeEach
	public void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver-0.30.0");
		// System.setProperty("webdriver.firefox.bin", "/usr/bin/firefox");
		driver = new FirefoxDriver();
	}

}
