package aula09br.ucsal.bes20211.bd2.aula09.business;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20211.bd2.aula09.business.CorrentistaBO;
import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

public class CorrentistaBOTest {

	private CorrentistaDAO correntistaDAOMock;

	private CorrentistaBO correntistaBO;

	@BeforeEach
	void setup() {
		correntistaDAOMock = Mockito.mock(CorrentistaDAO.class);
		correntistaBO = new CorrentistaBO(correntistaDAOMock);
	}

	@Test
	void testarIncluirCorrentistaValido() throws Exception {

		String nome = "Antonio Claudio Neiva";
		String telefone = "122323233";
		Integer anoNascimento = 2010;

		Mockito.when(correntistaDAOMock.findAll()).thenReturn(new ArrayList<>());

		// O método abrirConta é do tipo command, ou seja, não retorna nada (void).
		correntistaBO.abrirConta(nome, telefone, anoNascimento);

		Mockito.verify(correntistaDAOMock).persist(new Correntista(nome, telefone, anoNascimento));
	}

	/**
	 * Testar a inclusão de correntista cujo nome tenha tamanho abaixo do mínimo especificado.
	 * 
	 * @throws Exception
	 */
	@Test
	void testarIncluirCorrentistaNomePequeno() {
		String nome = "Neiva";
		String telefone = "71123123";
		Integer anoNascimento = 2000;

		String mensagemEsperada = "Nome inválido.";

		NegocioException negocioExceptionAtual = Assertions.assertThrows(NegocioException.class, () -> {
			correntistaBO.abrirConta(nome, telefone, anoNascimento);
		});

		Assertions.assertEquals(mensagemEsperada, negocioExceptionAtual.getMessage());
	}
}
