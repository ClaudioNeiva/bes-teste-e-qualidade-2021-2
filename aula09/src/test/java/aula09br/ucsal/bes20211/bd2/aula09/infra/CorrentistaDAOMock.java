package aula09br.ucsal.bes20211.bd2.aula09.infra;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

//Nós NÃO faremos assim na vida real. Esta classe é apenas um exemplo de como trabalhar SEM o uso de um framework de apoio a testes.
public class CorrentistaDAOMock extends CorrentistaDAO {

	private List<Correntista> correntistasConfigurados;

	private List<Correntista> chamadasPersist = new ArrayList<>();

	public void configurarCorrentistas(List<Correntista> correntistasConfigurados) {
		this.correntistasConfigurados = correntistasConfigurados;
	}

	@Override
	public List<Correntista> findAll() throws SQLException {
		return correntistasConfigurados;
	}

	@Override
	public void persist(Correntista correntista) throws SQLException {
		// Rastreando as chamadas ao persist.
		chamadasPersist.add(correntista);
	}

	public void validarChamadaPersist(Correntista correntista) throws Exception {
		if (!chamadasPersist.contains(correntista)) {
			throw new Exception("Chamada não registrada ao persist para o objeto " + correntista);
		}
	}

}
