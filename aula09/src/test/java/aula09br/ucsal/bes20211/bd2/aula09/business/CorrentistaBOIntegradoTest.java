package aula09br.ucsal.bes20211.bd2.aula09.business;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20211.bd2.aula09.business.CorrentistaBO;
import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

public class CorrentistaBOIntegradoTest {

	private CorrentistaDAO correntistaDAO;

	private CorrentistaBO correntistaBO;

	@BeforeEach
	void setup() {
		correntistaDAO = new CorrentistaDAO();
		correntistaBO = new CorrentistaBO(correntistaDAO);
	}

	@Test
	void testarIncluirCorrentistaValido() throws Exception {

		String nome = "Antonio Claudio Neiva";
		String telefone = "122323233";
		Integer anoNascimento = 2010;

		// O método abrirConta é do tipo command, ou seja, não retorna nada (void).
		correntistaBO.abrirConta(nome, telefone, anoNascimento);

		List<Correntista> correntistasAposAberturaConta = correntistaDAO.findAll();
		Assertions.assertTrue(correntistasAposAberturaConta.contains(new Correntista(nome, telefone, anoNascimento)));

	}
	
}
