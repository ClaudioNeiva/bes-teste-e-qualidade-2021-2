package aula09br.ucsal.bes20211.bd2.aula09.infra;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

// Nós NÃO faremos assim na vida real. Esta classe é apenas um exemplo de como trabalhar SEM o uso de um framework de apoio a testes.
public class CorrentistaDAOFake extends CorrentistaDAO {

	private HashMap<Integer, Correntista> correntistas = new HashMap<>();

	@Override
	public List<Correntista> findAll() throws SQLException {
		return (List<Correntista>) correntistas.values();
	}

	@Override
	public void persist(Correntista correntista) throws SQLException {
		correntistas.put(correntista.getId(), correntista);
	}

	@Override
	public void merge(Correntista correntistaMerge) throws SQLException {
		Correntista correntista = correntistas.get(correntistaMerge.getId());
		correntista.setNome(correntistaMerge.getNome());
		correntista.setTelefone(correntistaMerge.getTelefone());
		correntista.setAnoNascimento(correntistaMerge.getAnoNascimento());
	}

}
