package aula09br.ucsal.bes20211.bd2.aula09.business;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import aula09br.ucsal.bes20211.bd2.aula09.infra.CorrentistaDAOMock;
import br.ucsal.bes20211.bd2.aula09.business.CorrentistaBO;
import br.ucsal.bes20211.bd2.aula09.domain.Correntista;

public class CorrentistaBOSEMMokitoTest {

	private CorrentistaDAOMock correntistaDAOMock;

	private CorrentistaBO correntistaBO;

	@BeforeEach
	void setup() {
		correntistaDAOMock = new CorrentistaDAOMock();
		correntistaBO = new CorrentistaBO(correntistaDAOMock);
	}

	@Test
	void testarIncluirCorrentistaValido() throws Exception {

		String nome = "Antonio Claudio Neiva";
		String telefone = "122323233";
		Integer anoNascimento = 2010;

		correntistaDAOMock.configurarCorrentistas(new ArrayList<>());

		// O método abrirConta é do tipo command, ou seja, não retorna nada (void).
		correntistaBO.abrirConta(nome, telefone, anoNascimento);

		correntistaDAOMock.validarChamadaPersist(new Correntista(nome, telefone, anoNascimento));
		
	}
}
