package aula09br.ucsal.bes20211.bd2.aula09.infra;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

//Nós NÃO faremos assim na vida real. Esta classe é apenas um exemplo de como trabalhar SEM o uso de um framework de apoio a testes.
public class CorrentistaDAOStub extends CorrentistaDAO {

	private List<Correntista> correntistasConfigurados;

	public void configurarCorrentistas(List<Correntista> correntistasConfigurados) {
		this.correntistasConfigurados = correntistasConfigurados;
	}

	@Override
	public List<Correntista> findAll() throws SQLException {
		return correntistasConfigurados;
	}

	@Override
	public void persist(Correntista correntista) throws SQLException {
	}

}
