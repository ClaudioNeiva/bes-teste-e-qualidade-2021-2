package br.ucsal.bes20211.bd2.aula09.tui;

import java.sql.SQLException;
import java.util.Scanner;

import br.ucsal.bes20211.bd2.aula09.business.CorrentistaBO;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;

public class CorrentistaTUI {

	private Scanner scanner;
	private CorrentistaBO correntistaBO;

	public CorrentistaTUI(Scanner scanner, CorrentistaBO correntistaBO) {
		this.scanner = scanner;
		this.correntistaBO = correntistaBO;
	}

	public void cadastrar() throws NegocioException, SQLException {
		String nome = null;
		String telefone = null;
		Integer anoNascimento;

		System.out.println("Informe o nome: ");
		nome = scanner.nextLine();

		System.out.println("Informe o telefone: ");
		telefone = scanner.nextLine();

		System.out.println("Informe o ano de nascimento: ");
		anoNascimento = scanner.nextInt();

		correntistaBO.abrirConta(nome, telefone, anoNascimento);

	}

}
