package br.ucsal.bes20211.bd2.aula09.exception;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public NegocioException(String message) {
		super(message);
	}

}
